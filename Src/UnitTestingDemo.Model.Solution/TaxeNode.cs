using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model.Solution
{
    public class TaxeNode
    {
        private readonly IList<TaxeNode> _appliquerSur = new List<TaxeNode>();
        private readonly BaseTaxe _taxe;
        public TaxeNode(BaseTaxe taxe)
        {
            _taxe = taxe;            
        }
        public decimal Calculer(LineItem lineItem,decimal price)
        {
            decimal result = 0;
            foreach (var taxeNode in _appliquerSur)
            {
                result += taxeNode.Calculer(lineItem,price);
            }
            if (lineItem.Item.TaxesApplicables.Contains(_taxe))
            {
                decimal taxTotal = _taxe.Calculer(lineItem, price + result);
                lineItem.TaxeDetail.Add(_taxe, taxTotal);
                result += taxTotal;
            }
            return result;
        }
        public IList<TaxeNode> AppliquerSur
        {
            get
            {
                return _appliquerSur;
            }
        }
    }
}
