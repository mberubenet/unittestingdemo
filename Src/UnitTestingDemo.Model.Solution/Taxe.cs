﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model.Solution
{
    public class Taxe:BaseTaxe
    {
        public Taxe(string name)
            : base(name)
        {
            
        }        public decimal Taux { get; set; }
        
        protected internal override decimal calculerTaxe(LineItem lineItem,decimal prix)
        {
            return prix * Taux;
        }

    }
}
