﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model.Solution
{
    public class Facture
    {
        private readonly IList<LineItem> _lineItems = new List<LineItem>();

        public void Add(Item item, int Quantite)
        {
            _lineItems.Add(new LineItem(item, Quantite));
        }

        public IList<LineItem> LineItems
        {
            get
            {
                return _lineItems;
            }
        }

        public decimal GetTotal()
        {
            decimal result = 0;
            foreach (var item in _lineItems)
            {
                result += item.TotalPrice;
            }
            return result;
        }

        public Dictionary<BaseTaxe, decimal> GetTotalByTaxes()
        {
            var result = new Dictionary<BaseTaxe, decimal>();
            foreach (var item in _lineItems)
            {
                foreach (var taxeDetail in item.TaxeDetail)
                {
                    addAndSum(result, taxeDetail);
                }
            }
            return result;
        }

        private void addAndSum(Dictionary<BaseTaxe, decimal> result, KeyValuePair<BaseTaxe, decimal> taxeDetail)
        {
            if (!result.ContainsKey(taxeDetail.Key))
            {
                result.Add(taxeDetail.Key, taxeDetail.Value);
            }
            else
            {
                result[taxeDetail.Key] += taxeDetail.Value;
            }
        }
    }
}
