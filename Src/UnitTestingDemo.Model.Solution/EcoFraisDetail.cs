using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model.Solution
{
    public class EcoFraisDetail : BaseTaxe
    {
        public EcoFraisDetail(string name)
            : base(name)
        {

        }
        public decimal Montant { get; set; }
        public TypeItem AppliquerAuType { get; set; }

        protected internal override decimal calculerTaxe(LineItem lineItem, decimal prix)
        {
            if (lineItem.Item.TypeItem == AppliquerAuType)
            {
                return Montant * lineItem.Quantite;
            }
            return 0;
        }
    }
}
