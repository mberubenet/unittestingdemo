﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model.Solution
{
    public abstract class BaseTaxe
    {
        private readonly string _name;
        public BaseTaxe(string name)
        {
            _name = name;
        }
        public string Name
        {
            get
            {
                return _name;
            }
        }
        public decimal Calculer(LineItem lineItem, decimal sousTotal)
        {
            decimal result = 0;
            result = calculerTaxe(lineItem, sousTotal);
            return result;
        }

        protected internal abstract decimal calculerTaxe(LineItem lineItem, decimal prix);

    }
}
