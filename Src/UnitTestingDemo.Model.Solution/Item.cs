using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model.Solution
{
    public class Item
    {
        public decimal Prix { get; set; }
        public TypeItem TypeItem { get; set; }
        public Item()
        {
            TaxesApplicables = new List<BaseTaxe>();    
        }

        public IList<BaseTaxe> TaxesApplicables { get; private set; }
            
    }
}
