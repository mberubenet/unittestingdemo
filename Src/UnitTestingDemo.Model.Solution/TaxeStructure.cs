﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model.Solution
{
    public class TaxeStructure
    {
        private readonly Dictionary<string, IEnumerable<TaxeNode>> _nodes = new Dictionary<string, IEnumerable<TaxeNode>>();
        
        public void SetTaxeStructure(string region,IEnumerable<TaxeNode> taxeStructure)
        {
            region = region.ToLowerInvariant();
            if(_nodes.ContainsKey(region))
            {
                _nodes[region]=taxeStructure;
            }
            else{
                _nodes.Add(region,taxeStructure);
            }
        }

        public IEnumerable<TaxeNode> GetTaxeStructure(string region)
        {
            region = region.ToLowerInvariant();
            if(_nodes.ContainsKey(region))
            {
                return _nodes[region];
            }
            return Enumerable.Empty<TaxeNode>();
        }
    }
}
