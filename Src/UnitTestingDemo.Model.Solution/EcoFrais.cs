﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model.Solution
{
    public class EcoFrais:BaseTaxe
    {
        private readonly IList<EcoFraisDetail> _echoFraisDetails = new List<EcoFraisDetail>();
        public EcoFrais(string name)
            : base(name)
        {
            
        }

        protected internal override decimal calculerTaxe(LineItem lineItem,decimal prix)
        {
            decimal result = 0;
            foreach (var detail in EchoFraisDetails)
            {
                result += detail.calculerTaxe(lineItem, prix);
            }
            return result;
        }

        public IList<EcoFraisDetail> EchoFraisDetails
        {
            get
            {
                return _echoFraisDetails;
            }
        }
    }
}