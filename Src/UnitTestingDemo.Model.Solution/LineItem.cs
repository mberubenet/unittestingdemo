using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestingDemo.Model.Solution
{
    public class LineItem
    {
        private readonly Dictionary<BaseTaxe, decimal> _taxeDetail = new Dictionary<BaseTaxe, decimal>();

        public LineItem(Item item, int quantite)
        {
            Quantite = quantite;
            Item = item;            
        }

        public Item Item { get; private set; }
        public decimal SubTotalPrice
        {
            get
            {
                return Item.Prix * Quantite;
            }
        }
        public decimal TotalPrice
        {
            get
            {
                return SubTotalPrice+_taxeDetail.Sum(x=>x.Value);
            }
        }
        public int Quantite { get; private set; }

        public Dictionary<BaseTaxe, decimal> TaxeDetail
        {
            get
            {
                return _taxeDetail;
            }
        }

        
    }
}
