﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestingDemo.Model.Solution
{
    public class TaxCalculation
    {
        private readonly TaxeStructure _taxeStructure = new TaxeStructure();

        public TaxeStructure TaxeStructure
        {
            get
            {
                return _taxeStructure;
            }
        }

        public decimal CalculerTaxe(Item item, string region)
        {
            return CalculerTaxe(new LineItem(item, 1), region);
        }

        public decimal CalculerTaxe(LineItem item, string region)
        {
            decimal result = 0;
            foreach (var taxeNode in TaxeStructure.GetTaxeStructure(region))
            {
                result += taxeNode.Calculer(item,item.SubTotalPrice);
            }
            return result;
        }

        public decimal CalculerTaxe(Facture facture,string region)
        {
            return facture.LineItems.Sum(x => CalculerTaxe(x,region));
        }
    }
}
