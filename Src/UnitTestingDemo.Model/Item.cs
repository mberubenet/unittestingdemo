using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model
{
    public class Item
    {
        public decimal Prix { get; set; }
        public Item()
        {
            TaxesApplicables = new List<Taxe>();    
        }

        public IList<Taxe> TaxesApplicables { get; private set; }
            
    }
}
