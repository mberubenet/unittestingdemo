﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model
{
    public class TaxeNode
    {
        private readonly Taxe _taxe;
        private readonly IList<TaxeNode> _taxeNodes = new List<TaxeNode>();
        private static readonly Taxe _nullTaxe=new NullTaxe();

        public TaxeNode(Taxe taxe)
        {
            _taxe = taxe;
        }

        //Create a root node for same level taxes
        public TaxeNode()
            : this(_nullTaxe)
        {
            
        }

        public IList<TaxeNode> AppliquerSur
        {
            get
            {
                return _taxeNodes;
            }
        }
        public Taxe Taxe
        {
            get
            {
                return _taxe;
            }
        }
        public decimal CalculerTaxe(LineItem lineItem)
        {
            return internalCalculerTaxe(lineItem, lineItem.SubTotalPrice);
        }
        private decimal internalCalculerTaxe(LineItem lineItem,decimal price)
        {
            decimal subTaxes = 0;
            foreach (TaxeNode node in _taxeNodes)
            {
                subTaxes += node.internalCalculerTaxe(lineItem,price);
            }
            return this.Taxe.Calculer(lineItem, price + subTaxes) + subTaxes;
        }

        private class NullTaxe : Taxe
        {
            public NullTaxe()
                : base("NullTaxe")
            {
                
            }
            protected internal override decimal calculerTaxe(LineItem lineItem, decimal prix)
            {
                return 0;
            }
        }
    }
}
