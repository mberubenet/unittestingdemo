using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model
{
    public class TaxeFixe : Taxe
    {
        public TaxeFixe(string name)
            : base(name)
        {

        }
        public decimal Montant { get; set; }

        protected internal override decimal calculerTaxe(LineItem lineItem, decimal prix)
        {
            return Montant * lineItem.Quantite;
        }

        public override string ToString()
        {
            return string.Format("{0} : {1:C} par item", base.ToString(), Montant);
        }
    }
}
