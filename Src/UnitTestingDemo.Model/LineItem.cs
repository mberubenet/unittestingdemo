using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestingDemo.Model
{
    public class LineItem
    {
        private readonly TaxeCalculationResults _taxeDetail = new TaxeCalculationResults();

        public LineItem(Item item, int quantite)
        {
            Quantite = quantite;
            Item = item;            
        }

        public Item Item { get; private set; }
        public int Quantite { get; private set; }

        public TaxeCalculationResults TaxeDetail
        {
            get
            {
                return _taxeDetail;
            }
        }

        public decimal SubTotalPrice
        {
            get
            {
                return Item.Prix * Quantite;
            }
        }
        public decimal TotalPrice
        {
            get
            {
                return SubTotalPrice+_taxeDetail.Sum(x=>x.Value);
            }
        }
        

        
    }
}
