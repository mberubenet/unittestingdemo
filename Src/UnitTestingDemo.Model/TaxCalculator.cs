﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestingDemo.Model
{
    public class TaxCalculator
    {
        private readonly Dictionary<string, TaxeNode> _taxesByRegion = new Dictionary<string, TaxeNode>();

        public void SetTaxes(string region, Taxe taxe)
        {
            SetTaxes(region, new TaxeNode(taxe));
        }

        public void SetTaxes(Taxe taxe)
        {
            SetTaxes(string.Empty, new TaxeNode(taxe));
        }

        public void SetTaxes(string region, TaxeNode taxes)
        {
            region = region.ToLowerInvariant();
            if (_taxesByRegion.ContainsKey(region))
            {
                _taxesByRegion[region] = taxes;
            }
            else
            {
                _taxesByRegion.Add(region, taxes);
            }
        }

        private decimal CalculerTaxe(LineItem lineItem, string region)
        {
            region = region.ToLowerInvariant();
            TaxeNode taxes = _taxesByRegion.ContainsKey(region) ? _taxesByRegion[region] : null;
            decimal result = 0m;
            if (taxes != null)
            {
                result = taxes.CalculerTaxe(lineItem);
            }
            return result;
        }

        public decimal CalculerTaxe(Facture facture, string region)
        {
            return facture.LineItems.Sum(x => CalculerTaxe(x, region));
        }
    }
}
