﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model
{
    public abstract class Taxe
    {
        private readonly string _name;
        public Taxe(string name)
        {
            _name = name;
        }
        public string Name
        {
            get
            {
                return _name;
            }
        }
        public decimal Calculer(LineItem lineItem,decimal price)
        {
            if(lineItem.Item.TaxesApplicables.Contains(this))
            {
                return calculerTaxe(lineItem, price);
            }
            return 0;
        }

        protected internal abstract decimal calculerTaxe(LineItem lineItem, decimal prix);

        public override string ToString()
        {
            return _name;
        }
    }
}
