﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestingDemo.Model
{
    public class TaxePourc:Taxe
    {
        public TaxePourc(string name)
            : base(name)
        {
            
        }
        
        public decimal Taux { get; set; }
        
        protected internal override decimal calculerTaxe(LineItem lineItem,decimal prix)
        {
            return prix * Taux;
        }

        public override string ToString()
        {
            return string.Format("{0} : {1:P}",base.ToString(),Taux);
        }
    }
}
