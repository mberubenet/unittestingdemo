﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using UnitTestingDemo.Model.Solution;

namespace UnitTestingDemo.Model.Tests.Solution
{
    [TestClass]
    public class TaxCalculationFixtures
    {
        private Taxe _tps;
        private Taxe _tvq;
        private Taxe _tvo;
        private EcoFrais _ecoFrais;

        [TestInitialize]
        public void Setup()
        {
            _tps = new Taxe("TPS") { Taux = 0.05m };
            _tvq = new Taxe("TVQ") { Taux = 0.095m };
            _tvo = new Taxe("TVO") { Taux = 0.08m };

            _ecoFrais = new EcoFrais("EchoFrais");
            _ecoFrais.EchoFraisDetails.Add(new EcoFraisDetail("EchoFrais_Phone") { AppliquerAuType = TypeItem.cellPhone, Montant = 1.25m });
            _ecoFrais.EchoFraisDetails.Add(new EcoFraisDetail("EchoFrais_Computer") { AppliquerAuType = TypeItem.computer, Montant = 10m });
            _ecoFrais.EchoFraisDetails.Add(new EcoFraisDetail("EchoFrais_General") { AppliquerAuType = TypeItem.general, Montant = 0.5m });

        }
        [TestMethod]
        public void Calculer_taxes_sur_un_item()
        {
            //Arrange
            var sut = new TaxCalculation();
            Item item = new Item() { Prix = 100m };
            Taxe taxe1 = new Taxe("Taxe1") { Taux = 0.05m };
            item.TaxesApplicables.Add(taxe1);
            sut.TaxeStructure.SetTaxeStructure("QC", new List<TaxeNode>() { new TaxeNode(taxe1) });
            //Act
            decimal montantTaxe = sut.CalculerTaxe(item, "QC");
            //Assert
            Assert.AreEqual(5m, montantTaxe);
        }

        [TestMethod]
        public void Calculer_taxes_sur_une_liste_item()
        {
            //Arrange
            Taxe taxe1 = new Taxe("Taxe1") { Taux = 0.05m };
            Taxe taxe2 = new Taxe("Taxe2") { Taux = 0.15m };

            var sut = new TaxCalculation();
            sut.TaxeStructure.SetTaxeStructure("QC", new List<TaxeNode>() { new TaxeNode(taxe1),new TaxeNode(taxe2) });

            Item item1 = new Item() { Prix = 100m };
            item1.TaxesApplicables.Add(taxe1);

            Item item2 = new Item() { Prix = 100m };
            item2.TaxesApplicables.Add(taxe2);

            var facture = new Facture();
            facture.Add(item1, 1); //5$
            facture.Add(item2, 2); //30$
            //Act
            decimal montantTaxe = sut.CalculerTaxe(facture, "QC");
            //Assert
            Assert.AreEqual(35m, montantTaxe);
        }


        [TestMethod]
        public void Calculer_plusieurs_taxes_sur_une_liste()
        {
            //Arrange
            Taxe taxe1 = new Taxe("Taxe1") { Taux = 0.05m };

            Taxe taxe2 = new Taxe("Taxe2") { Taux = 0.10m };

            var sut = new TaxCalculation();
            sut.TaxeStructure.SetTaxeStructure("QC", new List<TaxeNode>() { new TaxeNode(taxe1), new TaxeNode(taxe2) });

            Item item1 = new Item() { Prix = 100m };
            item1.TaxesApplicables.Add(taxe1);

            Item item2 = new Item() { Prix = 100m };
            item2.TaxesApplicables.Add(taxe1);
            item2.TaxesApplicables.Add(taxe2);

            var facture = new Facture();
            facture.Add(item1, 1); //5$
            facture.Add(item2, 2); //30$
            //Act
            decimal montantTaxe = sut.CalculerTaxe(facture, "QC");
            
            //Assert
            Assert.AreEqual(35m, montantTaxe);
        }

        [TestMethod]
        public void Calculer_plusieurs_taxes__avec_Region_QC_sur_une_liste()
        {
            //Arrange
            Taxe taxe1 = new Taxe("Taxe1") { Taux = 0.05m };

            Taxe taxe2 = new Taxe("Taxe2") { Taux = 0.10m };

            Item item1 = new Item() { Prix = 100m };
            item1.TaxesApplicables.Add(taxe1);

            Item item2 = new Item() { Prix = 100m };
            item2.TaxesApplicables.Add(taxe1);
            item2.TaxesApplicables.Add(taxe2);

            var facture = new Facture();
            facture.Add(item1, 1); //5$
            facture.Add(item2, 2); //30$

            var sut = new TaxCalculation();
            var taxes = new List<TaxeNode>();
            taxes.Add(new TaxeNode(taxe1));
            taxes.Add(new TaxeNode(taxe2));
            sut.TaxeStructure.SetTaxeStructure("QC", taxes);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(facture,"QC");
            //Assert
            Assert.AreEqual(35m, montantTaxe);
        }

        [TestMethod]
        public void Calculer_plusieurs_taxes__avec_Region_ON_sur_une_liste()
        {
            //Arrange
            Taxe taxe1 = new Taxe("Taxe1") { Taux = 0.05m };

            Taxe taxe2 = new Taxe("Taxe2") { Taux = 0.10m };

            Item item1 = new Item() { Prix = 100m };
            item1.TaxesApplicables.Add(taxe1);

            Item item2 = new Item() { Prix = 100m };
            item2.TaxesApplicables.Add(taxe1);
            item2.TaxesApplicables.Add(taxe2);

            var facture = new Facture();
            facture.Add(item1, 1); //5$
            facture.Add(item2, 2); //10$

            var sut = new TaxCalculation();
            var taxesQC = new List<TaxeNode>();
            taxesQC.Add(new TaxeNode(taxe1));
            taxesQC.Add(new TaxeNode(taxe2));
            sut.TaxeStructure.SetTaxeStructure("QC", taxesQC);
            var taxesON = new List<TaxeNode>();
            taxesON.Add(new TaxeNode(taxe1));
            sut.TaxeStructure.SetTaxeStructure("ON", taxesON);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(facture, "ON");
            //Assert
            Assert.AreEqual(15m, montantTaxe);
        }

        [TestMethod]
        public void Calculer_avec_taxes_sur_sous_total_ON()
        {
            //Arrange

            Item item1 = new Item() { Prix = 100m };
            item1.TaxesApplicables.Add(_tps);
            item1.TaxesApplicables.Add(_tvo);

            var sut = new TaxCalculation();
            setupTaxesStandard(sut);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(item1, "ON");
            //Assert
            Assert.AreEqual(13m, montantTaxe);
        }

        [TestMethod]
        public void Calculer_avec_taxes_sur_sous_total_QC()
        {
            //Arrange

            Item item1 = new Item() { Prix = 100m };
            item1.TaxesApplicables.Add(_tps);
            item1.TaxesApplicables.Add(_tvq);

            var sut = new TaxCalculation();
            setupTaxesStandard(sut);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(item1, "QC");
            //Assert
            Assert.AreEqual(14.975m, montantTaxe);
        }

        [TestMethod]
        public void Calculer_avec_taxable_QC_pas_federal()
        {
            //Arrange

            Item item = new Item() { Prix = 100m };
            item.TaxesApplicables.Add(_tvq);

            var sut = new TaxCalculation();
            setupTaxesStandard(sut);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(item,"QC");
            //Assert
            Assert.AreEqual(9.5m, montantTaxe);
        }

        [TestMethod]
        public void Calculer_avec_taxable_federal_pas_QC()
        {
            //Arrange

            Item item = new Item() { Prix = 100m };
            item.TaxesApplicables.Add(_tps);

            var sut = new TaxCalculation();
            setupTaxesStandard(sut);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(item, "QC");
            //Assert
            Assert.AreEqual(5m, montantTaxe);
        }

        [TestMethod]
        public void Calculer_avec_taxable_EcoFrais()
        {
            //Arrange

            Item item1 = new Item() { Prix = 98.75m, TypeItem = TypeItem.cellPhone };
            item1.TaxesApplicables.Add(_ecoFrais);


            var sut = new TaxCalculation();
            setupTaxesEcoFrais(_ecoFrais, sut);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(item1, "QC");
            //Assert
            Assert.AreEqual(1.25m, montantTaxe);
        }

        [TestMethod]
        public void Calculer_avec_taxable_EcoFrais_nonApplicable()
        {
            //Arrange

            Item item1 = new Item() { Prix = 98.75m, TypeItem = TypeItem.food };
            item1.TaxesApplicables.Add(_ecoFrais);

            var sut = new TaxCalculation();
            setupTaxesEcoFrais(_ecoFrais, sut);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(item1, "QC");
            //Assert
            Assert.AreEqual(0m, montantTaxe);
        }

        [TestMethod]
        public void Calculer_avec_taxable_EcoFrais_avec_autre_taxes()
        {
            //Arrange

            Item item1 = new Item() { Prix = 98.75m, TypeItem = TypeItem.cellPhone };
            item1.TaxesApplicables.Add(_ecoFrais);
            item1.TaxesApplicables.Add(_tps);
            item1.TaxesApplicables.Add(_tvq);

            var sut = new TaxCalculation();
            setupTaxesEcoFrais(_ecoFrais, sut);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(item1, "QC");
            //Assert
            Assert.AreEqual(16.225m, montantTaxe);
        }


        [TestMethod]
        public void Calculer_avec_taxable_EcoFrais_avec_autre_taxes_ventilé()
        {
            //Arrange

            Item item1 = new Item() { Prix = 98.75m, TypeItem = TypeItem.cellPhone };
            item1.TaxesApplicables.Add(_ecoFrais);
            item1.TaxesApplicables.Add(_tps);
            item1.TaxesApplicables.Add(_tvq);
            var lineItem = new LineItem(item1, 1);

            var sut = new TaxCalculation();
            setupTaxesEcoFrais(_ecoFrais, sut);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(lineItem, "QC");
            //Assert
            Assert.AreEqual(16.225m, montantTaxe);
            Assert.AreEqual(1.25m, lineItem.TaxeDetail[_ecoFrais]);
            Assert.AreEqual(5m, lineItem.TaxeDetail[_tps]);
            Assert.AreEqual(9.975m, lineItem.TaxeDetail[_tvq]);
        }

        [TestMethod]
        public void Calculer_avec_taxable_EcoFrais_avec_autre_taxes_ventilé_plusieurs_quantite()
        {
            //Arrange

            Item item1 = new Item() { Prix = 98.75m, TypeItem = TypeItem.cellPhone };
            item1.TaxesApplicables.Add(_ecoFrais);
            item1.TaxesApplicables.Add(_tps);
            item1.TaxesApplicables.Add(_tvq);
            var lineItem = new LineItem(item1, 2);

            var sut = new TaxCalculation();
            setupTaxesEcoFrais(_ecoFrais, sut);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(lineItem, "QC");
            //Assert
            Assert.AreEqual(32.450m, montantTaxe);
            Assert.AreEqual(2.50m, lineItem.TaxeDetail[_ecoFrais]);
            Assert.AreEqual(10m, lineItem.TaxeDetail[_tps]);
            Assert.AreEqual(19.950m, lineItem.TaxeDetail[_tvq]);
        }


        [TestMethod]
        public void Calculer_avec_taxable_EcoFrais_avec_autre_taxes_ventilé_plusieurs_quantite_facture()
        {
            //Arrange

            Item item1 = new Item() { Prix = 98.75m, TypeItem = TypeItem.cellPhone };
            item1.TaxesApplicables.Add(_ecoFrais);
            item1.TaxesApplicables.Add(_tps);
            item1.TaxesApplicables.Add(_tvq);

            Item item2 = new Item() { Prix = 50m, TypeItem = TypeItem.cellPhone };
            item2.TaxesApplicables.Add(_ecoFrais);
            item2.TaxesApplicables.Add(_tps);
            item2.TaxesApplicables.Add(_tvq);

            var facture = new Facture();
            facture.Add(item1,2);
            facture.Add(item2,2);

            var sut = new TaxCalculation();
            setupTaxesEcoFrais(_ecoFrais, sut);
            //Act
            decimal montantTaxe = sut.CalculerTaxe(facture, "QC");
            //Assert
            //TODO : round
            AssertRound(50.30m, montantTaxe);
            AssertRound(2.50m, facture.LineItems[0].TaxeDetail[_ecoFrais]);
            AssertRound(10m, facture.LineItems[0].TaxeDetail[_tps]);
            AssertRound(19.95m, facture.LineItems[0].TaxeDetail[_tvq]);
            AssertRound(2.50m, facture.LineItems[1].TaxeDetail[_ecoFrais]);
            AssertRound(5.13m, facture.LineItems[1].TaxeDetail[_tps]);
            AssertRound(10.22m, facture.LineItems[1].TaxeDetail[_tvq]);
        }


        [TestMethod]
        public void Calculer_avec_taxable_EcoFrais_avec_taxe_hotel_ventilé_plusieurs_quantite_facture()
        {
            //Arrange
            Taxe taxeHoteliere = new Taxe("Hotel") { Taux = 0.02m };

            Item item1 = new Item() { Prix = 98.75m, TypeItem = TypeItem.cellPhone };
            item1.TaxesApplicables.Add(_ecoFrais);
            item1.TaxesApplicables.Add(_tps);
            item1.TaxesApplicables.Add(_tvq);

            Item item2 = new Item() { Prix = 50m, TypeItem = TypeItem.cellPhone };
            item2.TaxesApplicables.Add(_ecoFrais);
            item2.TaxesApplicables.Add(_tps);
            item2.TaxesApplicables.Add(_tvq);
            item2.TaxesApplicables.Add(taxeHoteliere);

            var facture = new Facture();
            facture.Add(item1, 2);
            facture.Add(item2, 2);

            var sut = new TaxCalculation();
            //Custom tax structure
            var taxeNodeHotel = new TaxeNode(taxeHoteliere);
            var taxesQC = new List<TaxeNode>();
            var taxeNodeTVQ = new TaxeNode(_tvq);
            var taxeNodeTPS = new TaxeNode(_tps);
            var taxeNodeEcoFrais = new TaxeNode(_ecoFrais);
            taxeNodeTVQ.AppliquerSur.Add(taxeNodeTPS);
            taxeNodeTPS.AppliquerSur.Add(taxeNodeEcoFrais);
            taxesQC.Add(taxeNodeTVQ);
            taxesQC.Add(taxeNodeHotel);
            sut.TaxeStructure.SetTaxeStructure("QC", taxesQC);
            var taxeNodeTVO = new TaxeNode(_tvo);
            var taxeNodeTPS_on = new TaxeNode(_tps);
            var taxesON = new List<TaxeNode>();
            taxesON.Add(taxeNodeTVO);
            taxesON.Add(taxeNodeTPS_on);
            sut.TaxeStructure.SetTaxeStructure("ON", taxesON);

            //Act
            decimal montantTaxe = sut.CalculerTaxe(facture, "QC");
            //Assert
            //TODO : round
            AssertRound(52.30m, montantTaxe);
            AssertRound(2.50m, facture.LineItems[0].TaxeDetail[_ecoFrais]);
            AssertRound(10m, facture.LineItems[0].TaxeDetail[_tps]);
            AssertRound(19.95m, facture.LineItems[0].TaxeDetail[_tvq]);
            AssertRound(2.50m, facture.LineItems[1].TaxeDetail[_ecoFrais]);
            AssertRound(5.13m, facture.LineItems[1].TaxeDetail[_tps]);
            AssertRound(10.22m, facture.LineItems[1].TaxeDetail[_tvq]);
            Assert.AreEqual(2m, facture.LineItems[1].TaxeDetail[taxeHoteliere]);
        }

        [TestMethod]
        public void Calculer_avec_taxable_EcoFrais_avec_taxe_hotel_dans_tvq_ventilé_plusieurs_quantite_facture()
        {
            //Arrange
            Taxe taxeHoteliere = new Taxe("Hotel") { Taux = 0.02m };

            Item item1 = new Item() { Prix = 98.75m, TypeItem = TypeItem.cellPhone };
            item1.TaxesApplicables.Add(_ecoFrais);
            item1.TaxesApplicables.Add(_tps);
            item1.TaxesApplicables.Add(_tvq);

            Item item2 = new Item() { Prix = 50m, TypeItem = TypeItem.cellPhone };
            item2.TaxesApplicables.Add(_ecoFrais);
            item2.TaxesApplicables.Add(_tps);
            item2.TaxesApplicables.Add(_tvq);
            item2.TaxesApplicables.Add(taxeHoteliere);

            var facture = new Facture();
            facture.Add(item1, 2);
            facture.Add(item2, 2);

            var sut = new TaxCalculation();
            //Custom tax structure
            var taxeNodeHotel = new TaxeNode(taxeHoteliere);
            var taxesQC = new List<TaxeNode>();
            var taxeNodeTVQ = new TaxeNode(_tvq);
            var taxeNodeTPS = new TaxeNode(_tps);
            var taxeNodeEcoFrais = new TaxeNode(_ecoFrais);
            taxeNodeTVQ.AppliquerSur.Add(taxeNodeTPS);
            taxeNodeTPS.AppliquerSur.Add(taxeNodeEcoFrais);
            taxeNodeTVQ.AppliquerSur.Add(taxeNodeHotel);
            taxesQC.Add(taxeNodeTVQ);
            sut.TaxeStructure.SetTaxeStructure("QC", taxesQC);
            var taxeNodeTVO = new TaxeNode(_tvo);
            var taxeNodeTPS_on = new TaxeNode(_tps);
            var taxesON = new List<TaxeNode>();
            taxesON.Add(taxeNodeTVO);
            taxesON.Add(taxeNodeTPS_on);
            sut.TaxeStructure.SetTaxeStructure("ON", taxesON);

            //Act
            decimal montantTaxe = sut.CalculerTaxe(facture, "QC");
            //Assert
            //TODO : round
            AssertRound(52.49m, montantTaxe);
            AssertRound(2.50m, facture.LineItems[0].TaxeDetail[_ecoFrais]);
            AssertRound(10m, facture.LineItems[0].TaxeDetail[_tps]);
            AssertRound(19.95m, facture.LineItems[0].TaxeDetail[_tvq]);
            AssertRound(2.50m, facture.LineItems[1].TaxeDetail[_ecoFrais]);
            AssertRound(5.13m, facture.LineItems[1].TaxeDetail[_tps]);
            AssertRound(10.41m, facture.LineItems[1].TaxeDetail[_tvq]);
            Assert.AreEqual(2m, facture.LineItems[1].TaxeDetail[taxeHoteliere]);
        }


        [TestMethod]
        public void Sommation_taxes_ventilées_plusieurs_quantite_facture()
        {
            //Arrange
            Taxe taxeHoteliere = new Taxe("Hotel") { Taux = 0.02m };

            Item item1 = new Item() { Prix = 98.75m, TypeItem = TypeItem.cellPhone };
            item1.TaxesApplicables.Add(_ecoFrais);
            item1.TaxesApplicables.Add(_tps);
            item1.TaxesApplicables.Add(_tvq);

            Item item2 = new Item() { Prix = 50m, TypeItem = TypeItem.cellPhone };
            item2.TaxesApplicables.Add(_ecoFrais);
            item2.TaxesApplicables.Add(_tps);
            item2.TaxesApplicables.Add(_tvq);
            item2.TaxesApplicables.Add(taxeHoteliere);

            var facture = new Facture();
            facture.Add(item1, 2);
            facture.Add(item2, 2);

            var sut = new TaxCalculation();
            //Custom tax structure
            var taxeNodeHotel = new TaxeNode(taxeHoteliere);
            var taxesQC = new List<TaxeNode>();
            var taxeNodeTVQ = new TaxeNode(_tvq);
            var taxeNodeTPS = new TaxeNode(_tps);
            var taxeNodeEcoFrais = new TaxeNode(_ecoFrais);
            taxeNodeTVQ.AppliquerSur.Add(taxeNodeTPS);
            taxeNodeTPS.AppliquerSur.Add(taxeNodeEcoFrais);
            taxeNodeTVQ.AppliquerSur.Add(taxeNodeHotel);
            taxesQC.Add(taxeNodeTVQ);
            sut.TaxeStructure.SetTaxeStructure("QC", taxesQC);
            var taxeNodeTVO = new TaxeNode(_tvo);
            var taxeNodeTPS_on = new TaxeNode(_tps);
            var taxesON = new List<TaxeNode>();
            taxesON.Add(taxeNodeTVO);
            taxesON.Add(taxeNodeTPS_on);
            sut.TaxeStructure.SetTaxeStructure("ON", taxesON);

            //Act
            decimal montantTaxe = sut.CalculerTaxe(facture, "QC");
            //Assert
            AssertRound(52.49m, montantTaxe);
            AssertRound(2.50m, facture.LineItems[0].TaxeDetail[_ecoFrais]);
            AssertRound(10m, facture.LineItems[0].TaxeDetail[_tps]);
            AssertRound(19.95m, facture.LineItems[0].TaxeDetail[_tvq]);
            AssertRound(2.50m, facture.LineItems[1].TaxeDetail[_ecoFrais]);
            AssertRound(5.13m, facture.LineItems[1].TaxeDetail[_tps]);
            AssertRound(10.41m, facture.LineItems[1].TaxeDetail[_tvq]);
            Assert.AreEqual(2m, facture.LineItems[1].TaxeDetail[taxeHoteliere]);
            //TOTAUX
            var taxeTotal = facture.GetTotalByTaxes();
            AssertRound(30.364m, taxeTotal[_tvq]);
            AssertRound(5m, taxeTotal[_ecoFrais]);
            AssertRound(15.13m, taxeTotal[_tps]);
            AssertRound(2m, taxeTotal[taxeHoteliere]);
            AssertRound(349.99m, facture.GetTotal());
        }

        private void AssertRound(decimal expected, decimal actual, int decimalPlaces = 2)
        {
            Assert.AreEqual(Decimal.Round(expected, decimalPlaces, MidpointRounding.AwayFromZero), Decimal.Round(actual, decimalPlaces, MidpointRounding.AwayFromZero));
        }

        private void setupTaxesStandard(TaxCalculation sut)
        {
            var taxesQC = new List<TaxeNode>();
            var taxeNodeTVQ = new TaxeNode(_tvq);
            var taxeNodeTPS = new TaxeNode(_tps);
            taxeNodeTVQ.AppliquerSur.Add(taxeNodeTPS);
            taxesQC.Add(taxeNodeTVQ);
            sut.TaxeStructure.SetTaxeStructure("QC", taxesQC);
            var taxeNodeTVO = new TaxeNode(_tvo);
            var taxesON = new List<TaxeNode>();
            taxesON.Add(taxeNodeTVO);
            taxesON.Add(taxeNodeTPS);
            sut.TaxeStructure.SetTaxeStructure("ON", taxesON);
        }

        private void setupTaxesEcoFrais(EcoFrais ecoFrais, TaxCalculation sut)
        {
            var taxesQC = new List<TaxeNode>();
            var taxeNodeTVQ = new TaxeNode(_tvq);
            var taxeNodeTPS = new TaxeNode(_tps);
            var taxeNodeEcoFrais = new TaxeNode(ecoFrais);
            taxeNodeTVQ.AppliquerSur.Add(taxeNodeTPS);
            taxeNodeTPS.AppliquerSur.Add(taxeNodeEcoFrais);
            taxesQC.Add(taxeNodeTVQ);
            sut.TaxeStructure.SetTaxeStructure("QC", taxesQC);
            var taxeNodeTVO = new TaxeNode(_tvo);
            var taxeNodeTPS_on = new TaxeNode(_tps);
            var taxesON = new List<TaxeNode>();
            taxesON.Add(taxeNodeTVO);
            taxesON.Add(taxeNodeTPS_on);
            sut.TaxeStructure.SetTaxeStructure("ON", taxesON);
        }
    }
}
