﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using UnitTestingDemo.Model.Solution;

namespace UnitTestingDemo.Model.Tests.Solution
{
    [TestClass]
    public class TaxeStructureFixture
    {
        [TestMethod]
        public void GetTaxStructure_that_exists()
        {
            //Arrange
            var sut = new TaxeStructure();
            var taxesNode = new List<TaxeNode>();
            sut.SetTaxeStructure("QC", taxesNode);
            //Act
            var result = sut.GetTaxeStructure("QC");
            //Assert
            Assert.AreSame(taxesNode, result);
        }

        [TestMethod]
        public void GetTaxStructure_that_not_exists()
        {
            //Arrange
            var sut = new TaxeStructure();
            var taxesNode = new List<TaxeNode>();
            sut.SetTaxeStructure("QC", taxesNode);
            //Act
            var result = sut.GetTaxeStructure("ON");
            //Assert
            Assert.AreNotSame(taxesNode, result);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count()==0);
        }

        [TestMethod]
        public void GetTaxStructure_many_exists()
        {
            //Arrange
            var sut = new TaxeStructure();
            var taxesNodeQC = new List<TaxeNode>();
            var taxesNodeON = new List<TaxeNode>();
            sut.SetTaxeStructure("QC", taxesNodeQC);
            sut.SetTaxeStructure("ON", taxesNodeON);
            //Act
            var result = sut.GetTaxeStructure("QC");
            //Assert
            Assert.AreSame(taxesNodeQC, result);
        }

        [TestMethod]
        public void GetTaxStructure_many_exists_ON()
        {
            //Arrange
            var sut = new TaxeStructure();
            var taxesNodeQC = new List<TaxeNode>();
            var taxesNodeON = new List<TaxeNode>();
            sut.SetTaxeStructure("QC", taxesNodeQC);
            sut.SetTaxeStructure("ON", taxesNodeON);
            //Act
            var result = sut.GetTaxeStructure("ON");
            //Assert
            Assert.AreSame(taxesNodeON, result);
        }

        [TestMethod]
        public void ReplaceExistingStructure()
        {
            //Arrange
            var sut = new TaxeStructure();
            var taxesNode = new List<TaxeNode>();
            sut.SetTaxeStructure("QC", taxesNode);
            var result = sut.GetTaxeStructure("QC");
            Assert.AreSame(taxesNode, result);
            var newTaxesNode = new List<TaxeNode>();
            //Act
            sut.SetTaxeStructure("QC", newTaxesNode);
            //Assert
            Assert.AreSame(newTaxesNode, sut.GetTaxeStructure("QC"));
        }

        [TestMethod]
        public void Set_Another_dont_Replace_Existing_Structure()
        {
            //Arrange
            var sut = new TaxeStructure();
            var taxesNode = new List<TaxeNode>();
            sut.SetTaxeStructure("QC", taxesNode);
            var result = sut.GetTaxeStructure("QC");
            Assert.AreSame(taxesNode, result);
            var newTaxesNode = new List<TaxeNode>();
            //Act
            sut.SetTaxeStructure("ON", newTaxesNode);
            //Assert
            Assert.AreSame(newTaxesNode, sut.GetTaxeStructure("ON"));
            Assert.AreSame(taxesNode, sut.GetTaxeStructure("QC"));
        }
    }
}
