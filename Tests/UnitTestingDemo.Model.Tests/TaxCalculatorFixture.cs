﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingDemo.Model.Tests
{
    [TestClass]
    public class TaxCalculatorFixture
    {
        //Step 1
        [TestMethod]
        public void CalculerTaxe_1_item_1_taxe()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe = new TaxePourc("tps"){Taux=0.05m};
            sut.SetTaxes(taxe);

            var item = new Item();
            item.Prix=100m;
            item.TaxesApplicables.Add(taxe);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item,1));

            //Act
            var result = sut.CalculerTaxe(facture, "");
            
            //Assert
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void CalculerTaxe_2_items_1_taxe()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe = new TaxePourc("tps") { Taux = 0.05m };
            sut.SetTaxes(taxe);

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(taxe);

            var item2 = new Item();
            item2.Prix = 50m;
            item2.TaxesApplicables.Add(taxe);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));
            facture.LineItems.Add(new LineItem(item2, 1));

            //Act
            var result = sut.CalculerTaxe(facture, "");

            //Assert
            Assert.AreEqual(7.5m, result);
        }

        [TestMethod]
        public void CalculerTaxe_2_items_0_taxe()
        {
            //Arrange
            var sut = new TaxCalculator();

            var item1 = new Item();
            item1.Prix = 100m;

            var item2 = new Item();
            item2.Prix = 50m;

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));
            facture.LineItems.Add(new LineItem(item2, 1));

            //Act
            var result = sut.CalculerTaxe(facture, "");

            //Assert
            Assert.AreEqual(0m, result);
        }

        [TestMethod]
        public void CalculerTaxe_0_items_1_taxe()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe = new TaxePourc("tps") { Taux = 0.05m };

            var facture = new Facture();

            //Act
            var result = sut.CalculerTaxe(facture, "");

            //Assert
            Assert.AreEqual(0m, result);
        }

        //Step 2
        [TestMethod]
        public void CalculerTaxe_1_item_2_taxes()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe1 = new TaxePourc("tps") { Taux = 0.05m };
            var taxe2 = new TaxePourc("tvo") { Taux = 0.08m };
            TaxeNode taxes = new TaxeNode();
            taxes.AppliquerSur.Add(new TaxeNode(taxe1));
            taxes.AppliquerSur.Add(new TaxeNode(taxe2));
            sut.SetTaxes("", taxes);

            var item = new Item();
            item.Prix = 100m;
            item.TaxesApplicables.Add(taxe1);
            item.TaxesApplicables.Add(taxe2);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item, 1));

            //Act
            var result = sut.CalculerTaxe(facture, "");

            //Assert
            Assert.AreEqual(13, result);
        }

        [TestMethod]
        public void CalculerTaxe_2_items_2_taxes()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe1 = new TaxePourc("tps") { Taux = 0.05m };
            var taxe2 = new TaxePourc("tvo") { Taux = 0.08m };
            TaxeNode taxes = new TaxeNode();
            taxes.AppliquerSur.Add(new TaxeNode(taxe1));
            taxes.AppliquerSur.Add(new TaxeNode(taxe2));
            sut.SetTaxes("", taxes);

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(taxe1);
            item1.TaxesApplicables.Add(taxe2);

            var item2 = new Item();
            item2.Prix = 50m;
            item2.TaxesApplicables.Add(taxe1);
            item2.TaxesApplicables.Add(taxe2);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));
            facture.LineItems.Add(new LineItem(item2, 1));

            //Act
            var result = sut.CalculerTaxe(facture, "");

            //Assert
            Assert.AreEqual(19.5m, result);
        }

        [TestMethod]
        public void CalculerTaxe_2_items_taxes_differentes()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe1 = new TaxePourc("tps") { Taux = 0.05m };
            var taxe2 = new TaxePourc("tvo") { Taux = 0.08m };
            TaxeNode taxes = new TaxeNode();
            taxes.AppliquerSur.Add(new TaxeNode(taxe1));
            taxes.AppliquerSur.Add(new TaxeNode(taxe2));
            sut.SetTaxes("", taxes);

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(taxe1);
            item1.TaxesApplicables.Add(taxe2);

            var item2 = new Item();
            item2.Prix = 50m;
            item2.TaxesApplicables.Add(taxe1);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));
            facture.LineItems.Add(new LineItem(item2, 1));

            //Act
            var result = sut.CalculerTaxe(facture, "");

            //Assert
            Assert.AreEqual(15.5m, result);
        }

        [TestMethod]
        public void CalculerTaxe_2_items_un_sans_taxe()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe1 = new TaxePourc("tps") { Taux = 0.05m };
            var taxe2 = new TaxePourc("tvo") { Taux = 0.08m };
            TaxeNode taxes = new TaxeNode();
            taxes.AppliquerSur.Add(new TaxeNode(taxe1));
            taxes.AppliquerSur.Add(new TaxeNode(taxe2));
            sut.SetTaxes("", taxes);

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(taxe1);
            item1.TaxesApplicables.Add(taxe2);

            var item2 = new Item();
            item2.Prix = 50m;

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));
            facture.LineItems.Add(new LineItem(item2, 1));

            //Act
            var result = sut.CalculerTaxe(facture, "");

            //Assert
            Assert.AreEqual(13m, result);
        }

        //Step 3
        [TestMethod]
        public void CalculerTaxe_1_item_taxe_par_region()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe1 = new TaxePourc("tvo") { Taux = 0.08m };
            var taxe2 = new TaxePourc("tvq") { Taux = 0.095m };
            sut.SetTaxes("QC", new TaxeNode(taxe2));
            sut.SetTaxes("ON", new TaxeNode(taxe1));

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(taxe1);
            item1.TaxesApplicables.Add(taxe2);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));

            //Act
            var resultQc = sut.CalculerTaxe(facture, "QC");
            var resultON = sut.CalculerTaxe(facture, "ON");

            //Assert
            Assert.AreEqual(9.5m, resultQc);
            Assert.AreEqual(8m, resultON);
        }

        [TestMethod]
        public void CalculerTaxe_1_item_taxe_par_region_taxe_globale()
        {
            //Arrange
            var sut = new TaxCalculator();
            var tvo = new TaxePourc("tvo") { Taux = 0.08m };
            var tvq = new TaxePourc("tvq") { Taux = 0.095m };
            var tps = new TaxePourc("tps") { Taux = 0.05m };
            TaxeNode tnQC = new TaxeNode();
            tnQC.AppliquerSur.Add(new TaxeNode(tvq));
            tnQC.AppliquerSur.Add(new TaxeNode(tps));
            sut.SetTaxes("QC", tnQC);
            TaxeNode tnON = new TaxeNode();
            tnON.AppliquerSur.Add(new TaxeNode(tvo));
            tnON.AppliquerSur.Add(new TaxeNode(tps));
            sut.SetTaxes("ON", tnON);

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(tvo);
            item1.TaxesApplicables.Add(tvq);
            item1.TaxesApplicables.Add(tps);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));

            //Act
            var resultQc = sut.CalculerTaxe(facture, "QC");
            var resultON = sut.CalculerTaxe(facture, "ON");

            //Assert
            Assert.AreEqual(14.5m, resultQc);
            Assert.AreEqual(13m, resultON);
        }

        //Step 4
        [TestMethod]
        public void CalculerTaxe_fixe_1_item()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe1 = new TaxeFixe("ecoFrais") { Montant= 2m };
            sut.SetTaxes(taxe1);

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(taxe1);
            
            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));

            //Act
            var result = sut.CalculerTaxe(facture,"");
            
            //Assert
            Assert.AreEqual(2m, result);
        }

        [TestMethod]
        public void CalculerTaxe_fixe_1_item_plusieurs_quantite()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe1 = new TaxeFixe("ecoFrais") { Montant = 2m };
            sut.SetTaxes(taxe1);

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(taxe1);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 2));

            //Act
            var result = sut.CalculerTaxe(facture, "");

            //Assert
            Assert.AreEqual(4m, result);
        }

        [TestMethod]
        public void CalculerTaxe_fixe_1_item_plusieurs_quantite_avec_Region()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe1 = new TaxeFixe("ecoFraisQC") { Montant = 2m };
            var taxe2 = new TaxeFixe("ecoFraisON") { Montant = 3m };
            sut.SetTaxes("QC", new TaxeNode(taxe1));
            sut.SetTaxes("ON", new TaxeNode(taxe2));

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(taxe1);
            item1.TaxesApplicables.Add(taxe2);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 2));

            //Act
            var resultQC = sut.CalculerTaxe(facture, "QC");
            var resultON = sut.CalculerTaxe(facture, "ON");

            //Assert
            Assert.AreEqual(4m, resultQC);
            Assert.AreEqual(6m, resultON);
        }

        [TestMethod]
        public void CalculerTaxe_fixe_combine_taxe_pourcent_1_item_plusieurs_quantite_avec_Region()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe1 = new TaxeFixe("ecoFraisQC") { Montant = 2m };
            var taxe2 = new TaxeFixe("ecoFraisON") { Montant = 3m };
            var taxe3 = new TaxePourc("tps") { Taux= 0.05m };
            TaxeNode tnQC = new TaxeNode();
            tnQC.AppliquerSur.Add(new TaxeNode(taxe1));
            tnQC.AppliquerSur.Add(new TaxeNode(taxe3));
            TaxeNode tnON = new TaxeNode();
            tnON.AppliquerSur.Add(new TaxeNode(taxe2));
            tnON.AppliquerSur.Add(new TaxeNode(taxe3));
            sut.SetTaxes("QC", tnQC);
            sut.SetTaxes("ON", tnON);


            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(taxe1);
            item1.TaxesApplicables.Add(taxe2);
            item1.TaxesApplicables.Add(taxe3);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 2));

            //Act
            var resultQC = sut.CalculerTaxe(facture, "QC");
            var resultON = sut.CalculerTaxe(facture, "ON");

            //Assert
            Assert.AreEqual(14m, resultQC);
            Assert.AreEqual(16m, resultON);
        }

        //Step 5-A
        [TestMethod]
        public void CalculerTaxe_appliquer_sur_autre_taxe()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe1 = new TaxePourc("tps") { Taux = 0.05m };
            var taxe2 = new TaxePourc("tvq") { Taux = 0.10m };
            //Comment appliquer la taxe2 sur taxe 1
            TaxeNode tvqTN = new TaxeNode(taxe2);
            tvqTN.AppliquerSur.Add(new TaxeNode(taxe1));
            sut.SetTaxes("", tvqTN);

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(taxe1);
            item1.TaxesApplicables.Add(taxe2);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));

            //Act
            var result = sut.CalculerTaxe(facture,"");

            //Assert
            Assert.AreEqual(15.5m, result);
        }

        [TestMethod]
        public void CalculerTaxe_appliquer_sur_autre_taxe_avec_region()
        {
            //Arrange
            var sut = new TaxCalculator();
            var taxe1 = new TaxePourc("tps") { Taux = 0.05m };
            var taxe2 = new TaxePourc("tvq") { Taux = 0.10m };
            var taxe3 = new TaxePourc("tvo") { Taux = 0.08m };
            //Comment appliquer la taxe2 sur taxe 1
            TaxeNode tnQC = new TaxeNode(taxe2);
            tnQC.AppliquerSur.Add(new TaxeNode(taxe1));
            TaxeNode tnON = new TaxeNode();
            tnON.AppliquerSur.Add(new TaxeNode(taxe1));
            tnON.AppliquerSur.Add(new TaxeNode(taxe3));
            sut.SetTaxes("QC", tnQC);
            sut.SetTaxes("ON", tnON);

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(taxe1);
            item1.TaxesApplicables.Add(taxe2);
            item1.TaxesApplicables.Add(taxe3);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));

            //Act
            var resultQC = sut.CalculerTaxe(facture, "QC");
            var resultON = sut.CalculerTaxe(facture, "ON");

            //Assert
            Assert.AreEqual(15.5m, resultQC);
            Assert.AreEqual(13m, resultON);
        }

        //Step 5-B
        [TestMethod]
        public void CalculerTaxe_appliquer_sur_autres_taxes()
        {
            //Arrange
            var sut = new TaxCalculator();
            var ecoFrais = new TaxeFixe("echoFrais") { Montant = 2m };
            var tps = new TaxePourc("tps") { Taux = 0.05m };
            var tvq = new TaxePourc("tvq") { Taux = 0.10m };
            //Comment appliquer la taxe2 sur taxe 1
            TaxeNode tn = new TaxeNode(tvq);
            TaxeNode tpsTn = new TaxeNode(tps);
            tpsTn.AppliquerSur.Add(new TaxeNode(ecoFrais));
            tn.AppliquerSur.Add(tpsTn);
            sut.SetTaxes("", tn);

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(tps);
            item1.TaxesApplicables.Add(tvq);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));

            //Act
            var result = sut.CalculerTaxe(facture, "");

            //Assert
            Assert.AreEqual(15.5m, result);
        }

        [TestMethod]
        public void CalculerTaxe_appliquer_sur_autres_taxes_no_TPS()
        {
            //Arrange
            var sut = new TaxCalculator();
            var ecoFrais = new TaxeFixe("echoFrais") { Montant = 2m };
            var tps = new TaxePourc("tps") { Taux = 0.05m };
            var tvq = new TaxePourc("tvq") { Taux = 0.10m };
            //Comment appliquer la taxe2 sur taxe 1
            TaxeNode tn = new TaxeNode(tvq);
            TaxeNode tpsTn = new TaxeNode(tps);
            tpsTn.AppliquerSur.Add(new TaxeNode(ecoFrais));
            tn.AppliquerSur.Add(tpsTn);
            sut.SetTaxes("", tn);

            var item1 = new Item();
            item1.Prix = 100m;
            item1.TaxesApplicables.Add(ecoFrais);
            item1.TaxesApplicables.Add(tvq);

            var facture = new Facture();
            facture.LineItems.Add(new LineItem(item1, 1));

            //Act
            var result = sut.CalculerTaxe(facture, "");

            //Assert
            Assert.AreEqual(12.2m, result);
        }
    }
}
