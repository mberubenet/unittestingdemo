﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTestingDemo.Model.Tests
{
    [TestClass]
    public class DemoInjectionAvecMock
    {
        [TestMethod]
        public void FormatDate_Today()
        {
            //Arrange
            Mock<IDateTimeProvider> mockDTP = new Mock<IDateTimeProvider>();
            //TODO : ???
            Mock<ILogger> mockLog = new Mock<ILogger>();
            var sut = new MyClassAvecMock(mockDTP.Object,mockLog.Object);
            //Act
            var result = sut.FormatDate(new DateTime(2013, 11, 11));
            //Assert
            Assert.AreEqual("Today", result);
        }

        [TestMethod]
        public void CheckIfEven()
        {
            //Arrange
            Mock<IDateTimeProvider> mockDTP = new Mock<IDateTimeProvider>();
            Mock<ILogger> mockLog = new Mock<ILogger>();
            var sut = new MyClassAvecMock(mockDTP.Object, mockLog.Object);
            //Act
            sut.CheckIfEven(2);
            //Assert
            //TODO : ???
            Assert.Inconclusive();
        }
    }

    public class MyClassAvecMock
    {
        private readonly ILogger _logger;
        private readonly IDateTimeProvider _dateTimeProvider;
        public MyClassAvecMock(IDateTimeProvider dateTimeProvider, ILogger logger)
        {
            _dateTimeProvider = dateTimeProvider;
            _logger = logger;
        }
        public string FormatDate(DateTime dateToFormat)
        {
            if (dateToFormat.Date == _dateTimeProvider.GetNow().Date)
            {
                return "Today";
            }
            return dateToFormat.ToShortDateString();
        }

        public void CheckIfEven(int value)
        {
            if (value % 2 == 0)
            {
                _logger.Log("Even");
            }
            else
            {
                _logger.Log("Odd");
            }
        }
    }
}
