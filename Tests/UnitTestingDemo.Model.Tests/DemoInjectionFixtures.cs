﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingDemo.Model.Tests
{
    [TestClass]
    public class DemoInjectionFixtures
    {
        [TestClass]
        public class MyClassFixture
        {
            [TestMethod]
            public void Test_FormatDate_sans_injection()
            {
                //Arrange
                var sut = new MyClass();
                var currentDate = DateTime.Now.Date;
                //Act
                var dateString = sut.FormatDate(currentDate);
                //Assert
                Assert.AreEqual("Today", dateString);
                //Question : Que peut-il arriver si on roule le test de nuit ???
            }

            [TestMethod]
            public void Test_CheckIfEvent_sans_injection()
            {
                //Arrange
                var sut = new MyClass();
                //Act
                sut.CheckIfEven(2);
                //Assert
                //Comment on test ???
            }
        }

        #region avec better
        [TestClass]
        public class MyClassBetterFixture
        {
            [TestMethod]
            public void Test_FormatDate_sans_injection()
            {
                //Arrange
                var sut = new MyClassBetter();
                var currentDate = DateTime.Now.Date;
                //Act
                var dateString = sut.FormatDate(currentDate);
                //Assert
                Assert.AreEqual("Today", dateString);
                //Question : Que peut-il arriver si on roule le test de nuit ???
            }

            [TestMethod]
            public void Test_CheckIfEvent_sans_injection()
            {
                //Arrange
                var sut = new MyClassBetter();
                //Act
                sut.CheckIfEven(2);
                //Assert
                //Comment on test ???
            }
        }
        #endregion

        #region Avec Best
        [TestClass]
        public class MyClassRefactoredFixtures
        {
            [TestMethod]
            public void Test_FormatDate_avec_injection()
            {
                //Arrange
                var now = new DateTime(2013, 11, 05, 14, 15, 00);
                var sut = new MyClassRefactored(new FakeDateTimeProvider(now), new FakeLogger());
                var currentDate = DateTime.Now.Date;
                //Act
                var dateString = sut.FormatDate(new DateTime(2013, 11, 05, 08, 15, 00));
                //Assert
                //Va toujours fonctionner car dépendance éliminée
                Assert.AreEqual("Today", dateString);
            }

            [TestMethod]
            public void Test_CheckIfEvent_avec_injection()
            {
                //Arrange
                var now = new DateTime(2013, 11, 05, 14, 15, 00);
                FakeLogger logger = new FakeLogger();
                var sut = new MyClassRefactored(new FakeDateTimeProvider(now), logger);
                //Act
                sut.CheckIfEven(2);
                //Assert
                Assert.AreEqual("Even", logger.LastMessage);
            }

            private class FakeDateTimeProvider : IDateTimeProvider
            {
                private readonly DateTime _dateTimeToReturn;
                public FakeDateTimeProvider(DateTime dateTimeToReturn)
                {
                    _dateTimeToReturn = dateTimeToReturn;
                }
                public DateTime GetNow()
                {
                    return _dateTimeToReturn;
                }
            }

            private class FakeLogger : ILogger
            {
                public string LastMessage { get; private set; }
                public void Log(string message)
                {
                    LastMessage = message;
                }
            }
        }
        #endregion


    }

    public class MyClass
    {
        public string FormatDate(DateTime dateToFormat)
        {
            if (dateToFormat.Date == DateTime.Now.Date)
            {
                return "Today";
            }
            return dateToFormat.ToShortDateString();
        }

        public void CheckIfEven(int value)
        {
            if (value % 2 == 0)
            {
                Console.WriteLine("Even");
            }
            else
            {
                Console.WriteLine("Odd");
            }
        }
    }

    #region better
    public class MyClassBetter
    {
        private readonly DateTimeProvider _dateTimeProvider = new DateTimeProvider();
        private readonly ConsoleLogger _consoleLogger = new ConsoleLogger();

        public string FormatDate(DateTime dateToFormat)
        {
            if (dateToFormat.Date == _dateTimeProvider.GetNow().Date)
            {
                return "Today";
            }
            return dateToFormat.ToShortDateString();
        }

        public void CheckIfEven(int value)
        {
            if (value % 2 == 0)
            {
                _consoleLogger.Log("Even");
            }
            else
            {
                _consoleLogger.Log("Odd");
            }
        }
    }


    public class DateTimeProvider
    {
        public DateTime GetNow()
        {
            return DateTime.Now;
        }
    }

    public class ConsoleLogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
    #endregion

    #region best
    public class MyClassRefactored
    {
        private readonly ILogger _logger;
        private readonly IDateTimeProvider _dateTimeProvider;
        public MyClassRefactored(IDateTimeProvider dateTimeProvider, ILogger logger)
        {
            _dateTimeProvider = dateTimeProvider;
            _logger = logger;
        }
        public string FormatDate(DateTime dateToFormat)
        {
            if (dateToFormat.Date == _dateTimeProvider.GetNow().Date)
            {
                return "Today";
            }
            return dateToFormat.ToShortDateString();
        }

        public void CheckIfEven(int value)
        {
            if (value % 2 == 0)
            {
                _logger.Log("Even");
            }
            else
            {
                _logger.Log("Odd");
            }
        }
    }

    public interface IDateTimeProvider
    {
        DateTime GetNow();
    }
    public interface ILogger
    {
        void Log(string message);
    }

    public class ConcreteDateTimeProvider : IDateTimeProvider
    {
        public DateTime GetNow()
        {
            return DateTime.Now;
        }
    }

    public class ConcreteConsoleLogger : ILogger
    {

        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
    #endregion
}
