﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingDemo.Model.Tests
{
    [TestClass]
    public class TestDemoException
    {
        [TestMethod]
        public void Test_Add_2_chiffres()
        {
            //Arrange
            var sut = new Calculator();
            //Act
            var result = sut.Add(2, 3);
            //Assert
            Assert.AreEqual(5, result);
        }

        [TestMethod,ExpectedException(typeof(DivideByZeroException))]
        public void Test_Divide_by_0()
        {
            //Arrange
            var sut = new Calculator();
            //Act
            var result = sut.Divide(2, 0);
            //Assert
            // Not required, expectedException
        }


        [TestMethod]
        public void Test_Divide_by_0_with_AssertThrows()
        {
            //Arrange
            var sut = new Calculator();
            //Act
            Exception result = AssertThrows<DivideByZeroException>(()=>sut.Divide(2, 0));
            //Assert
            // Pas nécessaire à moins de vouloir tester les propriétés de l'exception retournée
        }


        [TestMethod]
        public void Test_Divide_by_0_with_customAssert()
        {
            //Arrange
            var Assert = new myAssert();
            var sut = new Calculator();
            //Act
            Exception result = Assert.Throws<DivideByZeroException>(() => sut.Divide(2, 0));
            //Assert
            // Pas nécessaire à moins de vouloir tester les propriétés de l'exception retournée
        }

        private static T AssertThrows<T>(Action codeExecute) where T:Exception
        {
            try
            {
                codeExecute();
            }
            catch (T e)
            {
                //Exception lancée. Ok. Si une exception d'un autre type est lancée, le test échoue
                return e;
            }
            Assert.Fail(string.Format("Exception de type {0} non lancée",typeof(T).Name));
            return null;
        }
    }

    public class Calculator
    {
        public int Add(int value1, int value2)
        {
            return value1 + value2;
        }

        public int Divide(int numerator, int denominator)
        {
            return numerator / denominator;
        }
    }
}
