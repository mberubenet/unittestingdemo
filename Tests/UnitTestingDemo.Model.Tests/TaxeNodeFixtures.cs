﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingDemo.Model.Tests
{
    [TestClass]
    public class TaxeNodeFixtures
    {
        [TestMethod]
        public void TestNodes()
        {
            //Arrange
            var tps = new TaxePourc("tps") { Taux = 0.05m };
            var tvq = new TaxePourc("tvq") { Taux = 0.10m };
            var ecoFrais = new TaxeFixe("ecoFrais") { Montant = 2m };

            var sut = new TaxeNode(tvq);
            var taxeNodeTPS = new TaxeNode(tps);
            sut.AppliquerSur.Add(taxeNodeTPS);
            var taxeNodeEcoFrais = new TaxeNode(ecoFrais);
            taxeNodeTPS.AppliquerSur.Add(taxeNodeEcoFrais);

            
            Item item = new Item();
            item.Prix = 100m;
            item.TaxesApplicables.Add(tvq);
            item.TaxesApplicables.Add(tps);
            item.TaxesApplicables.Add(ecoFrais);

            LineItem lineItem = new LineItem(item,1);

            //Act
            decimal result = sut.CalculerTaxe(lineItem);
            //Assert
            Assert.AreEqual(17.81m, result);
        }

        [TestMethod]
        public void TestNodes_plusieurs_même_niveau()
        {
            //Arrange
            var tps = new TaxePourc("tps") { Taux = 0.05m };
            var tvq = new TaxePourc("tvq") { Taux = 0.10m };
            var ecoFrais = new TaxeFixe("ecoFrais") { Montant = 2m };
            var taxeDeLuxe = new TaxeFixe("taxeDeLuxe") { Montant = 10m };

            var sut = new TaxeNode(tvq);
            var taxeNodeTPS = new TaxeNode(tps);
            sut.AppliquerSur.Add(taxeNodeTPS);
            var taxeNodeEcoFrais = new TaxeNode(ecoFrais);
            var taxeNodeLuxe = new TaxeNode(taxeDeLuxe);
            taxeNodeTPS.AppliquerSur.Add(taxeNodeEcoFrais);
            taxeNodeTPS.AppliquerSur.Add(taxeNodeLuxe);


            Item item = new Item();
            item.Prix = 100m;
            item.TaxesApplicables.Add(tvq);
            item.TaxesApplicables.Add(tps);
            item.TaxesApplicables.Add(ecoFrais);
            item.TaxesApplicables.Add(taxeDeLuxe);

            LineItem lineItem = new LineItem(item, 1);

            //Act
            decimal result = sut.CalculerTaxe(lineItem);
            //Assert
            Assert.AreEqual(29.36m, result);
        }

        [TestMethod]
        public void TestNodes_some_not_applicable()
        {
            //Arrange
            var tps = new TaxePourc("tps") { Taux = 0.05m };
            var tvq = new TaxePourc("tvq") { Taux = 0.10m };
            var ecoFrais = new TaxeFixe("ecoFrais") { Montant = 2m };

            var sut = new TaxeNode(tvq);
            var taxeNodeTPS = new TaxeNode(tps);
            sut.AppliquerSur.Add(taxeNodeTPS);
            var taxeNodeEcoFrais = new TaxeNode(ecoFrais);
            taxeNodeTPS.AppliquerSur.Add(taxeNodeEcoFrais);


            Item item = new Item();
            item.Prix = 100m;
            item.TaxesApplicables.Add(tvq);
            item.TaxesApplicables.Add(ecoFrais);

            LineItem lineItem = new LineItem(item, 1);

            //Act
            decimal result = sut.CalculerTaxe(lineItem);
            //Assert
            Assert.AreEqual(12.2m, result);
        }

        [TestMethod]
        public void TestNodes_multipleQuantity()
        {
            //Arrange
            var tps = new TaxePourc("tps") { Taux = 0.05m };
            var tvq = new TaxePourc("tvq") { Taux = 0.10m };
            var ecoFrais = new TaxeFixe("ecoFrais") { Montant = 2m };

            var sut = new TaxeNode(tvq);
            var taxeNodeTPS = new TaxeNode(tps);
            sut.AppliquerSur.Add(taxeNodeTPS);
            var taxeNodeEcoFrais = new TaxeNode(ecoFrais);
            taxeNodeTPS.AppliquerSur.Add(taxeNodeEcoFrais);


            Item item = new Item();
            item.Prix = 100m;
            item.TaxesApplicables.Add(tvq);
            item.TaxesApplicables.Add(tps);
            item.TaxesApplicables.Add(ecoFrais);

            LineItem lineItem = new LineItem(item, 2);

            //Act
            decimal result = sut.CalculerTaxe(lineItem);
            //Assert
            Assert.AreEqual(35.62m, result);
        }
    }
}
