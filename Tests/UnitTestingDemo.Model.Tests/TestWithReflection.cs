﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingDemo.Model.Tests
{
    [TestClass]
    public class TestWithReflection
    {
        [TestMethod]
        public void Test_IncrementCounter()
        {
            //Arrange
            var sut = new VisitCounter();
            //Act
            sut.IncrementCounter();
            //Assert
            Assert.AreEqual(1, sut.GetType().GetField("_counter", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(sut));
        }
    }

    public class VisitCounter
    {
        private int _counter = 0;
        
        public void IncrementCounter()
        {
            _counter++;
        }
    }
}
