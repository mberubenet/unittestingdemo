﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingDemo.Model.Tests
{
    [TestClass]
    public class CustomAsserts
    {
        private readonly myAssert Assert = new myAssert();

        [TestMethod]
        public void Test_InBetween()
        {
            //Arrange
            var sut = new DateTime(2013, 11, 11);
            //Act
            var result = sut.AddDays(1);
            //Assert
            Assert.IsBetween(result,new DateTime(2013, 11, 11),new DateTime(2013, 11, 13));
            Assert.AreEqual(12, result.Day);
        }
    }

    public class myAssert
    {
        public void IsBetween(DateTime actual,DateTime expectedLow, DateTime expectedHigh)
        {
            IsTrue(actual>=expectedLow && actual<=expectedHigh,string.Format("{0} not between {1} and {2}",actual,expectedLow,expectedHigh));
        }

        public void IsTrue(bool condition, string message)
        {
            Assert.IsTrue(condition, message);
        }

        public void AreEqual(object expected,object actual)
        {
            Assert.AreEqual(expected, actual);
        }

        public T Throws<T>(Action codeExecute) where T:Exception
        {
            try
            {
                codeExecute();
            }
            catch (T e)
            {
                //Exception lancée. Ok. Si une exception d'un autre type est lancée, le test échoue
                return e;
            }
            Assert.Fail(string.Format("Exception de type {0} non lancée", typeof(T).Name));
            return null;
        }
    
    }
}
